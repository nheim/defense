#!/usr/bin/env python
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from netCDF4 import Dataset
from torsk.visualize import animate_imshow


parser = argparse.ArgumentParser()
parser.add_argument('--save', type=str, help='save animation to mp4', default='')
args = parser.parse_args()


with Dataset('data/world_SSH.nc', 'r') as ds:
    ssh = ds['SSH'][:200]
    ssh.mask = np.logical_or(ssh.mask, ssh == -1)
    time = ds['time'][:]

plt.imshow(ssh[0, ::-1], cmap='inferno')
plt.tight_layout()
if args.save: plt.savefig('world.pdf', transparent=True)
plt.show()

anim = animate_imshow(
        ssh[:, ::-1], cmap_name='inferno', time=time/365., figsize=(8, 4))

if args.save:
    writer = animation.writers['ffmpeg'](fps=30)
    anim.save(args.save, writer=writer, dpi=200)

plt.show()
