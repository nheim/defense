#!/usr/bin/env python
import argparse
import glob

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import seaborn as sns

from torsk.analysis import read_summary
from torsk.models import utils

sns.set_style('whitegrid')

parser = argparse.ArgumentParser()
parser.add_argument('path', type=str, help='path to model output')
parser.add_argument('--save', type=str, help='save animation to mp4', default='')
args = parser.parse_args()

summary_path = glob.glob(args.path + '/*tfevent*')[0]
summary = read_summary(summary_path)
params = utils.load_setup_params(args.path)
eq_frames = params['nr_equilibration_frames']
pr_frames = params['nr_prev_frames']

target = np.squeeze(summary['target'])
prediction = np.squeeze(summary['prediction'])
loss = np.concatenate((np.ones(target.shape[1]), summary['loss']), axis=0)
metric = np.concatenate((np.ones(target.shape[1]), summary['metric']), axis=0)

state_indices = [10, 20, 30, 40]
enc_states = summary['all_enc_states'][:, :, state_indices]
dec_states = summary['all_dec_states'][:, :, state_indices]
states = np.concatenate((enc_states, dec_states), axis=1)
states = np.swapaxes(states, 1, 2)
del summary

fig, ax = plt.subplots(3, 1)

# fill backgrounds
ax[0].fill_between([0, eq_frames], [-0.1, -0.1], [1.1, 1.1], color='C3', alpha=0.2)
ax[0].fill_between([eq_frames, pr_frames], [-0.1, -0.1], [1.1, 1.1], color='C2', alpha=0.2)
ax[1].fill_between([0, eq_frames], [-1.1, -1.1], [1.1, 1.1], color='C3', alpha=0.2, label='equil.')
ax[1].fill_between([eq_frames, pr_frames], [-1.1, -1.1], [1.1, 1.1], color='C2', alpha=0.2, label='train')
# draw eq/pred lines
ax[0].plot([eq_frames, eq_frames], [-0.1, 1.1], color='black')
ax[0].plot([pr_frames, pr_frames], [-0.1, 1.1], color='black')
ax[1].plot([eq_frames, eq_frames], [-1.1, 1.1], color='black')
ax[1].plot([pr_frames, pr_frames], [-1.1, 1.1], color='black')

time = np.arange(target.shape[1])
line_p, = ax[0].plot(time, prediction[0], label='pred.')
line_t, = ax[0].plot(time, target[0], '-.', label='targ.')
lines_state = [ax[1].plot(s)[0] for s in states[0]]
line_l, = ax[2].plot(-time[::-1], loss[:target.shape[1]], label='Train Loss')
line_m, = ax[2].plot(-time[::-1], loss[:target.shape[1]], label='Pred. Loss')

ax[0].set_ylabel('Output')
ax[1].set_ylabel('States')
ax[2].set_xlabel('Timesteps')
ax[0].legend(frameon=True, loc='upper center')
ax[1].legend(frameon=True, loc='upper center')
ax[2].legend(frameon=True, loc='upper center')

ax[0].autoscale(axis='both', tight=True)
ax[1].autoscale(axis='both', tight=True)
ax[2].autoscale(axis='x', tight=True)
ax[2].set_yscale('log')
plt.tight_layout()

def animate(i):
    line_t.set_ydata(target[i])
    line_p.set_ydata(prediction[i])
    for line, state in zip(lines_state, states[i]):
        line.set_ydata(state)
    lo = loss[i:i+target.shape[1]]
    me = metric[i:i+target.shape[1]]
    line_l.set_ydata(lo)
    line_m.set_ydata(me)
    ax[2].set_ylim(0.5*lo.min(), 2*me.max())
    return (line_t, line_p) + tuple(lines_state) + (line_l, line_m)

# Init only required for blitting to give a clean slate.
def init():
    ary = np.ma.array(time, mask=True)
    line_t.set_ydata(ary)
    line_p.set_ydata(ary)
    for line in lines_state:
        line.set_ydata(ary)
    line_l.set_ydata(ary)
    line_m.set_ydata(ary)
    return (line_t, line_p) + tuple(lines_state) + (line_l, line_m)

frames = np.arange(1, target.shape[0])
anim = animation.FuncAnimation(
    fig, animate, frames, init_func=init, interval=25, blit=False)
if args.save:
    writer = animation.writers['ffmpeg'](fps=30)
    anim.save(args.save, writer=writer, dpi=200)
plt.show()
