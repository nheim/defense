#!/bin/bash
mpv --loop world.mp4
mpv --loop kuro.mp4
mpv --loop --speed 0.5 sine_good.mp4
mpv --loop --speed 0.5 super_sine.mp4
mpv --loop --speed 0.5 mackey_500_linear.mp4
mpv --loop --speed 0.5 step_bad.mp4
mpv --loop --speed 0.5 step_good.mp4
mpv --loop --speed 0.5 mackey_500.mp4
mpv --loop --speed 0.7 kuro_prediction.mp4
