#!/usr/bin/env python
import argparse
import glob

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation

from torsk.analysis import read_summary, read_tensor

def _blit_draw(self, artists, bg_cache):
    # Handles blitted drawing, which renders only the artists given instead
    # of the entire figure.
    updated_ax = []
    for a in artists:
        # If we haven't cached the background for this axes object, do
        # so now. This might not always be reliable, but it's an attempt
        # to automate the process.
        if a.axes not in bg_cache:
            # bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.bbox)
            # change here
            bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.figure.bbox)
        a.axes.draw_artist(a)
        updated_ax.append(a.axes)

    # After rendering all the needed artists, blit each axes individually.
    for ax in set(updated_ax):
        # and here
        # ax.figure.canvas.blit(ax.bbox)
        ax.figure.canvas.blit(ax.figure.bbox)
matplotlib.animation.Animation._blit_draw = _blit_draw

parser = argparse.ArgumentParser()
parser.add_argument('path', type=str, help='path to model output')
parser.add_argument('--save', type=str, help='save animation to mp4', default='')
args = parser.parse_args()

summary_path = glob.glob(args.path + '/*tfevent*')[0]
varspec = {'prediction':read_tensor, 'target':read_tensor}
summary = read_summary(summary_path, varspec=varspec)

prediction = summary['prediction']
target = summary['target'][:, -prediction.shape[1]:]
japan = np.load('japan.npy')[::-1]
japan = np.repeat([japan], repeats=target.shape[1], axis=0)

# best around 315
idx = 315

frames1 = np.ma.masked_array(target[idx][:, ::-1], mask=japan)
frames2 = np.ma.masked_array(prediction[idx][:, ::-1], mask=japan)
vmin = target.min()
vmax = target.max()
cmap_name = 'inferno'

fig, ax = plt.subplots(1, 2, figsize=(12, 4))
im1  = ax[0].imshow(frames1[0], animated=True, vmin=vmin, vmax=vmax,
                cmap=plt.get_cmap(cmap_name))
im2  = ax[1].imshow(frames2[0], animated=True, vmin=vmin, vmax=vmax,
                cmap=plt.get_cmap(cmap_name))
ax[0].set_title('Target')
ax[1].set_title('Prediciton')
plt.colorbar(im2)
text = ax[0].text(1.20, 1.05, '', transform = ax[0].transAxes, va='center')
time = np.arange(len(frames1))

def init():
    text.set_text("")
    im1.set_data(frames1[0])
    im2.set_data(frames2[0])
    return im1,im2,text

def animate(i):
    text.set_text(f'Step: {time[i]}')
    im1.set_data(frames1[i])
    im2.set_data(frames2[i])
    return im1,im2,text

anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=len(frames1), interval=20, blit=True)

if args.save:
    writer = animation.writers['ffmpeg'](fps=30)
    anim.save(args.save, writer=writer, dpi=200)

plt.show()
