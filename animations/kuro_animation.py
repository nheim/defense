#!/usr/bin/env python
import argparse
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import torsk.datasets.ocean as ds
from torsk.visualize import animate_imshow


parser = argparse.ArgumentParser()
parser.add_argument('--save', type=str, help='save animation to mp4', default='')
args = parser.parse_args()


sli = slice(0, -1)
time, ssh = ds._read_kuro(sli, sli, demask=False)
anim = animate_imshow(
        ssh[:, ::-1], cmap_name='inferno', time=time/365., figsize=(8, 4))

if args.save:
    writer = animation.writers['ffmpeg'](fps=30)
    anim.save(args.save, writer=writer, dpi=200)

plt.show()
