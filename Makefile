TARGETS = main.pdf
SOURCES = $(shell find . -name '*.tex')

main.pdf: main.tex $(SOURCES)
	latexmk -xelatex -use-make main.tex

clean:
	latexmk -CA
	rm main.nav main.snm

open:
	evince $(TARGETS) &
