import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from netCDF4 import Dataset

from torsk.visualize import animate_imshow

def remove_ticks(fig):
    for i, ax in enumerate(fig.axes):
        for tl in ax.get_xticklabels() + ax.get_yticklabels():
            tl.set_visible(False)
        ax.xaxis.set_ticks_position('none')
        ax.yaxis.set_ticks_position('none')



with Dataset('shallow.nc', 'r') as src:
    eta = src['eta'][:]
    x = src['x'][:]
    y = src['y'][:]

eta = eta[200]

fig, ax = plt.subplots(1, 1, figsize=plt.figaspect(.5))
x = 0
for e in eta[:eta.shape[0]//2]:
    ax.plot(e-x, color='C0')
    x += 0.06

ax.set_xlim(0, 99)
#ax.set_ylim(0.0, -4.9)
remove_ticks(fig)
ax.autoscale(axis='both', tight=True)
plt.savefig('shallow.pdf', transparent=True)
plt.show()
