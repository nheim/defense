import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--save", default=False, action="store_true")
parser.add_argument("--no-show", default=False, action="store_true")
args = parser.parse_args()


sns.set_style("whitegrid")
sns.set_context("notebook")


def make_sequence():
    np.random.seed(1)
    start = 610; end = 640;
    x = np.linspace(0, 4*np.pi, 1000)
    t = np.arange(start, end)
    x = np.sin(x) #+ np.random.normal(size=x.shape[0], scale=0.7)
    y = x.copy()
    a = 6*np.cos(x[start:end]* 4 + 3.0) - 3.95
    x[start:end] = a
    x += np.random.normal(size=x.shape, scale=0.1)
    x = 0.7+0.5*x
    y = 0.7+0.5*y
    return x, y

labels, prediction = make_sequence()

w, h = plt.figaspect(0.6)
fig, ax = plt.subplots(1, 1, sharex=True, figsize=(w, h/2.))
ax.plot(labels, ".-", label=r"Truth")
ax.plot(prediction, label=r"Prediciton")
ax.plot(np.abs(labels - prediction), label=r"Error")
ax.legend(frameon=True)
plt.tight_layout()
if args.save: plt.savefig("anomaly.pdf", transparent=True)
if not args.no_show: plt.show()
plt.close()
