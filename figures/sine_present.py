#!/usr/bin/env python
import argparse
import glob

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import seaborn as sns

from torsk.analysis import read_summary
from torsk.models import utils

sns.set_style('whitegrid')
sns.set_context('notebook')

parser = argparse.ArgumentParser()
parser.add_argument('path', type=str, help='path to model output')
parser.add_argument('--save', type=str, help='save plots', default='')
args = parser.parse_args()

summary_path = glob.glob(args.path + '/*tfevent*')[0]
summary = read_summary(summary_path)
params = utils.load_setup_params(args.path)


idx = 200
prediction = summary['prediction'][:idx, 0, 0]
target = summary['target'][:idx, 0, 0]

w, h = plt.figaspect(0.6)
plt.subplots(1, 1, figsize=(w, h/2))
plt.plot(target, label=r'Target $\vec{d}_t$')
plt.plot(prediction, label=r'Prediction $\vec{y}_t$')
plt.legend(frameon=True)
plt.xlabel("Training steps")
plt.tight_layout()
if args.save:
    plt.savefig(args.save, transparent=True)
plt.show()

